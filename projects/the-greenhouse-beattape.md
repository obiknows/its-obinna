---
thumbnail: "/uploads/the-greenhouse-beattape-front.png"
title: The Greenhouse Beat Tape
date: 2019-06-01 23:00:00 +0000
categories:
- beats
- music
- design
project_bg_color: '#59786B'
project_fg_color: "#FFFFFF"

---
This is my first beat tape. 

Listen [here](https://soundcloud.com/o13nna/sets/the-greenhouse-beat-tape) on Soundcloud (https://soundcloud.com/o13nna/sets/the-greenhouse-beat-tape)

# Album Art
### Front Cover
![](/uploads/the-greenhouse-beattape-front.png)

### Back Cover
![](/uploads/the-greenhouse-beattape-back.png)