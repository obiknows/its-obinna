---
thumbnail: "/uploads/electric-seamoss-screenshot.png"
title: Electric Seamoss
date: 2019-06-14 23:00:00 +0000
categories:
- ecommerce
- design
- development
- webflow 
project_bg_color: '#665AA1'
project_fg_color: '#E9D15E'
---

This was a website done for a health and wellness brand. The website was constructed in Webflow. 

See it [here](https://electricseamoss.com)

![](/uploads/electric-seamoss-screenshot.png)
