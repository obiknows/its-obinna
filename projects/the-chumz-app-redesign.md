---
thumbnail: "/uploads/the-chumz-app-icon.png"
title: The Chumz App Redesign
date: 2019-01-20 23:00:00 +0000
categories:
- design
- development
- mobile app
project_bg_color: ''
project_fg_color: "#FBC843"

---

This is an design overhaul done for an ecommerce app called Chumz. 

This project finished design in January
and is currently in development for an August 2019 release.


# BEFORE


# AFTER 

![](/uploads/the-chumz-app-icon.png)


![](/uploads/the-chumz-redesign-doc.png)